import React, {useEffect, useState} from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import {createMuiTheme, makeStyles, ThemeProvider} from '@material-ui/core';
import Header from './components/header/Header';
import 'fontsource-open-sans';
import 'fontsource-oswald';
import Footer from './components/footer/Footer';
import ScrollTop from './components/scrollTop/ScrollTop';
import PublicLoader from './components/classBuilder/PublicLoader';
import NotFound from "./pages/notFound/NotFound";
import Auth from '@aws-amplify/auth';
import AuthForms from "./pages/auth/AuthForms";
import {UserInfoContext} from "./service/UserInfoContext";
import GradeList from "./pages/gradeList/GradeList";
import UnitList from "./pages/unitList/UnitList";
import SnackBarAlert from "./components/snackbarAlert/SnackbarAlert";
import Subjects from './pages/subjects/Subjects';
import {DOCENTES_URL, ESTUDIANTES_URL, FORM_TYPE_SIGN_IN, FORM_TYPE_SIGNED_IN} from "./utils/constants";
import StudentList from './pages/StudentList/StudentList';
import StudyProgramGradeList from "./pages/studyProgramGradeList/StudyProgramGradeList";
import UnitProgramList from './pages/unitProgram/UnitProgramList';
import StudyProgram from "./pages/studyProgram/StudyProgram";
import TermsConditions from './pages/termsConditions/TermsConditions';
import ModalAlert from './components/modalAlert/ModalAlert';



const theme = createMuiTheme({
    typography: {
        fontFamily: [
            'Open-Sans',
            'Rubik',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif'
        ].join(','),
    }
    
});

const useStyles = makeStyles(theme => ({
    offset: theme.mixins.toolbar,
    appBarSpacer: theme.mixins.toolbar,
    app: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100%',
    },
    sdContent: {
        flex: '1'
    },
    footer: {
        padding: theme.spacing(3, 2),
        marginTop: '5em',
        backgroundColor: 'white',

    },
}));

const formInitialState = {
    username: '',
    password: '',
    email: '',
    authCode: '',
    formType: null,
    newPassword: '',
    cognitoUser: null,
    processing: false
};

function App() {
    const [formState, updateFormState] = useState(formInitialState);
    const [userInfoState, updateUserInfoState] = useState({});
    const classes = useStyles();

    useEffect(() => {
        Auth.currentSession().then((rsp) => {
            updateFormState({...formState, formType: rsp ? FORM_TYPE_SIGNED_IN : FORM_TYPE_SIGN_IN})
        }).catch(() => {
            updateFormState({...formState, formType: FORM_TYPE_SIGN_IN})
        });
    }, [])

    return (

        <UserInfoContext.Provider value={{state: userInfoState, update: updateUserInfoState}}>
            <ThemeProvider theme={theme}>
                {
                    formState.formType === FORM_TYPE_SIGNED_IN ? (
                        <div className={classes.app}>
                            <Router onUpdate={() => window.scrollTo(0, 0)}
                                    basename={`${process.env.REACT_APP_PUBLIC_URL}`}>
                                <ScrollTop/>
                                <ModalAlert/>
                                <Header formState={formState} updateFormState={updateFormState}/>
                                <Switch>
                                    <Route exact
                                           path={`/${DOCENTES_URL}/:section/:grade/:unit/:class/:subject/:lesson/lista`}
                                           component={StudentList}/>
                                    <Route exact path={`/${DOCENTES_URL}/:section/:grade/:unit/:class/:subject/:lesson`}
                                           component={PublicLoader}/>
                                    <Route exact
                                           path={`/${ESTUDIANTES_URL}/:section/:grade/:unit/:class/:subject/:lesson`}
                                           component={PublicLoader}/>
                                    <Route exact path={`/${DOCENTES_URL}/:section/:grade/:unit/:class`}
                                           component={Subjects}/>
                                    <Route exact path={`/${ESTUDIANTES_URL}/:section/:grade/:unit/:class`}
                                           component={Subjects}/>
                                    <Route exact path={`/${ESTUDIANTES_URL}/:section/:grade`} component={UnitList}/>
                                    <Route exact path={`/${DOCENTES_URL}/:section/:grade`} component={UnitList}/>
                                    <Route exact path={`/programadeestudio`} component={StudyProgramGradeList}/>
                                    <Route exact path={`/programadeestudio/:grade`} component={UnitProgramList}/>
                                    <Route exact path={`/programadeestudio/:grade/:unit`} component={StudyProgram}/>
                                    <Route exact path={`/`} component={GradeList}/>
                                    <Route exact path={`/terminosycondiciones`} component={TermsConditions}/>
                                    <Route component={NotFound}/>
                                </Switch>
                                <SnackBarAlert/>
                                <Footer/>
                            </Router>
                        </div>
                    ) : <AuthForms formState={formState} updateFormState={updateFormState}/>
                }
            </ThemeProvider>
        </UserInfoContext.Provider>
    );
}

export default App;
