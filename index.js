import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import Auth from '@aws-amplify/auth';
import config from './amplify-config';
import {Provider} from "react-redux";
import store from "./utils/store";

Auth.configure(config);

ReactDOM.render(
 
      <Provider store={store}>
          <App/>
      </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
