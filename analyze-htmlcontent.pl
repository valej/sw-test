no warnings 'experimental';
# use List::MoreUtils qw(uniq);

sub uniq {
    my %seen;
    return grep { !$seen{$_}++ } @_;
}


#=================================================================
# ----VERSION 2-----
# How to generate "outout":
#
# In Windows :  dir /s/b *.html > outout
# In Linux   :  find . -name \*.html -print | sort > /tmp/outout
#=================================================================
 
my $files = 'outout';
open my $filesHandler, $files or die "Could not open $files: $!";
while (my $line = <$filesHandler>) {
    if ($ARGV[0]) {
        if (index($line,  $ARGV[0])>=1) {
           print $line;;
           $line =~ s/\\/\//g;
           $line =~ s/\n//g;
           eachFile($line);
        }
     }else {
        print $line;;
        $line =~ s/\\/\//g;
        $line =~ s/\n//g;
        eachFile($line);
     }
}
close $filesHandler;

sub eachFile {
    my $file = $_[0];

    #================
    # Check filename
    #================

    my @m = $file =~ /^.*?htmlcontent\/(\w+)\/(\w+)\/(\w+)\/(\w+)\/(g\d_u\d+_)(com|vid|cie|mat|int|edu)(_)(\d+|apt)\.html$/;

    if (@m . length() eq 0) {
        print "  ERROR: unexpected path\n";
        return;
    }

    my $yearFromPath;
    if (@m[0] eq "primergrado") {$yearFromPath = "g1";}
    elsif (@m[0] eq "segundogrado") {$yearFromPath = "g2";}
    elsif (@m[0] eq "tercergrado") {$yearFromPath = "g3";}
    elsif (@m[0] eq "cuartogrado") {$yearFromPath = "g4"}
    elsif (@m[0] eq "quintogrado") {$yearFromPath = "g5";}
    elsif (@m[0] eq "sextogrado") {$yearFromPath = "g6";}
    else {
        print "  ERROR: unknown year folder: @m[0]\n";
        return;
    }

    my $unitFromPath;
    if (@m[1] ~~ /unidad\d+/) {$unitFromPath = @m[1] =~ s/unidad(\d+)/u$1/r;}
    else {
        print "  ERROR: unknown unit folder: @m[1]\n";
        return;
    }

    my $classFromPath;
    if (@m[2] eq 'aperturadeunidad') {$classFromPath = "apt";}
    elsif (@m[2] ~~ /clase\d+/) {$classFromPath = @m[2] =~ s/clase(\d+)/$1/r;}
    else {
        print "  ERROR: unknown class folder: @m[2]\n";
        return;
    }

    my $subjectFromPath;
    if (@m[3] eq "comunicacion") {$subjectFromPath = "com";}
    elsif (@m[3] eq "matematica") {$subjectFromPath = "mat";}
    elsif (@m[3] eq "medionaturalysalud") {$subjectFromPath = "cie";}
    elsif (@m[3] eq "interioridad") {$subjectFromPath = "int";}
    elsif (@m[3] eq "cienciassociales") {$subjectFromPath = "cie";}
    elsif (@m[3] eq "cienciasnaturalesysalud") {$subjectFromPath = "cie";}
    elsif (@m[3] eq "cienciasnaturales") {$subjectFromPath = "cie";}
    elsif (@m[3] eq "vidasocialytrabajo") {$subjectFromPath = "vid";}
    elsif (@m[3] eq "educacionparalasalud") {$subjectFromPath = "edu";}
    else {
        print "  ERROR: unknown subject folder: @m[3]\n";
        return;
    }

    my $lessonNameBuiltFromPath = "$yearFromPath\_$unitFromPath\_$subjectFromPath\_$classFromPath";
    my $lessonName = "@m[4]@m[5]@m[6]@m[7]";

    if ($lessonName ne $lessonNameBuiltFromPath) {
        print "  ERROR: lesson name does not match the folders\n";
        return;
    }

    #====================
    # Check file content
    #====================

    open my $fileHandler, '<', $file or die "error opening $file: $!";
    my $data = do {
        local $/;
        <$fileHandler>
    };
    close $fileHandler;

    $data =~ s/\s+/ /gm;

    my @exerciseIds = $data =~ /<interactive[^>]+id_tag=["']([^"']+)["']/g;
    my @exercises = $data =~ /<interactive/g;
    if (@exercises . length() != @exerciseIds . length()) {
        print "  ERROR: some exercises without id_tag\n";
    }
    my @uniqueExerciseIds = uniq(@exerciseIds);
    if (@uniqueExerciseIds . length() != @exerciseIds . length()) {
        print "  ERROR: duple exercise IDs within the same lesson\n";
        print STDERR ($lessonNameBuiltFromPath, " ERROR: duple exercise IDs within the same lesson, verify resultado-del-analisis\n");
        die "Hay ID tags duplicados"
    }
  
    for (@exerciseIds) {
        my $previous = 0;
        if ((not $_ ~~ /^g\d_u\d+_(com|vid|cie|mat|int|edu)_(\d+|apt)_ex_\d+$/) && (not $_ ~~ /^g\d_u\d+_(com|vid|cie|mat|int|edu)_(\d+|apt)_ex_\d+_\d+$/) && (not $_ ~~ /^g\d_u\d+_(com|vid|cie|mat|int|edu)_(\d+|apt)_ex_\d+_\d+_\d+$/))  {
            $previous = printExerciseError($previous, "does not fit the expected format");
           
        }
        if (not $_ ~~ /^$lessonName/) {
            $previous = printExerciseError($previous, "does not start with lesson name");
        }
        if ($previous gt 0) {
            print "\n";
        }
    }
}

sub printExerciseError {
    if ($_[0] eq 0) {
        print "  ERROR: exercise ID '$_': $_[1]";
    }
    else {
        print ", $_[1]";
    }
    return $_[0] + 1;
}

